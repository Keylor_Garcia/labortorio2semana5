/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2semana5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import static laboratorio2semana5.Laboratorio2Semana5.listaAdopcion;
import static laboratorio2semana5.Laboratorio2Semana5.listaPersonas;

/**
 *
 * @author usuario
 */
public class adopcionMascota {

    public void adopcion_mascota() {

        ArrayList listaLocalAdopcion = new ArrayList<>();//En esta parte quiero guardar la informaicion de los animales adoptados.

        /*
        El estado de esa mascota pasa a Adoptado, para que eventualmente cuando otra persona ingrese al sistema no pueda
adoptar dicha mascota.
         */
        Scanner ingreso = new Scanner(System.in);//Se necesita esto para agregar los datos de consola.
        Scanner ingreso_enteros = new Scanner(System.in);//Se necesita esto para agregar los datos de consola.

        //Guardar los datos
        for (int j = 0; j <= Laboratorio2Semana5.listaPersonas.size() - 1; j++) {
            PaqueteObjetosPersonas datos = Laboratorio2Semana5.listaPersonas.get(j);
            int ced = datos.getCedulaPersona();
            String nom = datos.getNombrePersona();
            String gen = datos.getGeneroPersona();
            //System.out.println("\nDatos de las personas que se encuentran registradas.");
            System.out.println("Cédula de la persona: " + ced + ", Nombre de la persona: " + nom + ", Género de la persona: " + gen);
            System.out.println("\n");

        }

        System.out.print("Digite la cédula de la persona: ");//Solicito el dato
        int cedula = ingreso.nextInt();//Guardar los datos
        boolean esta_cedula_persona = false;

        for (int g = 0; g <= Laboratorio2Semana5.listaPersonas.size() - 1; g++) {

            PaqueteObjetosPersonas datos = Laboratorio2Semana5.listaPersonas.get(g);

            if (cedula == datos.getCedulaPersona()) {
                listaLocalAdopcion.add(cedula);

                listaLocalAdopcion.add(datos.getNombrePersona());//Separ los datos de las personas y de los animales.
                listaLocalAdopcion.add(datos.getGeneroPersona());

                for (int j = 0; j <= Laboratorio2Semana5.listaMascotas.size() - 1; j++) {
                    PaqueteDeObjetosMascotas datos3 = Laboratorio2Semana5.listaMascotas.get(j);
                    int cod = datos3.getIdMascota();
                    String nom = datos3.getNombreDeMascota();
                    String tipo = datos3.getTipoDeMascota();
                    String est = datos3.getEstado();
                    if (est == "Disponible") {//En esta parte valido si la mascota esta disponible o no.
                        //System.out.println("\nDatos de las mascotas que se encuentran registradas");
                        System.out.println("Código de la mascota: " + cod + ", Nombre de la mascota: " + nom + ", Tipo de mascota: " + tipo + ", estado: " + est);
                        System.out.println("\n");
                    }

                }
                System.out.print("\nDigite el código de la mascota: ");//Solicito el dato

                int codigo = ingreso_enteros.nextInt();//Guardar los datos
                boolean esta_codigo_animal = false;

                for (int j = 0; j <= Laboratorio2Semana5.listaMascotas.size() - 1; j++) {
                    PaqueteDeObjetosMascotas datos1 = Laboratorio2Semana5.listaMascotas.get(j);

                    if (codigo == datos1.getIdMascota()) {
                        esta_codigo_animal = true;
                        listaLocalAdopcion.add(codigo);
                        listaLocalAdopcion.add(datos1.getNombreDeMascota());
                        listaLocalAdopcion.add(datos1.getTipoDeMascota());

                        datos1.setEstado("Adoptado");
                        listaLocalAdopcion.add(datos1.getEstado());

                        System.out.println(datos1.getNombreDeMascota() + " fue adoptada con éxito.");

                    }

                }
                if (esta_codigo_animal == false) {
                    System.out.println("Dato no válido");

                }
                esta_cedula_persona = true;

            } //else {
            //System.out.println("Dato no válido");
            //}

        }
        if (esta_cedula_persona == false) {
            System.out.println("Dato no válido");

        }

        listaAdopcion.add(listaLocalAdopcion);

    }

}
