/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2semana5;

/**
 *
 * @author usuario
 */
public class PaqueteDeObjetosMascotas {

    private int idMascota;
    private String nombreDeMascota;
    private String tipoDeMascota;
    private String estado;

    public PaqueteDeObjetosMascotas(int idMascota, String nombreDeMascota, String tipoDeMascota, String estado) {
        this.idMascota = idMascota;
        this.nombreDeMascota = nombreDeMascota;
        this.tipoDeMascota = tipoDeMascota;
        this.estado = estado;
    }

    public int getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(int idMascota) {
        this.idMascota = idMascota;
    }

    public String getNombreDeMascota() {
        return nombreDeMascota;
    }

    public void setNombreDeMascota(String nombreDeMascota) {
        this.nombreDeMascota = nombreDeMascota;
    }

    public String getTipoDeMascota() {
        return tipoDeMascota;
    }

    public void setTipoDeMascota(String tipoDeMascota) {
        this.tipoDeMascota = tipoDeMascota;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
